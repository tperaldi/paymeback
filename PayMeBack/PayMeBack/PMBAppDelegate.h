//
//  PMBAppDelegate.h
//  PayMeBack
//
//  Created by tatiana peraldi on 19/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMBAppDelegate : UIResponder <UIApplicationDelegate>
{
    UINavigationController *navigationController;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;

@end
