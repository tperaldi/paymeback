//
//  MembersListController.m
//  PayMeBack
//
//  Created by tatiana peraldi on 21/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//


#import "MembersListController.h"

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

@interface MembersListController ()

@end

@implementation MembersListController


@dynamic addButton;
@synthesize delegate;
@synthesize contacts;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        sharedStore = [Store sharedStore];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Set the title.
    self.title = @"Members";
    
    //set background color
    self.tableView.backgroundColor = [UIColor underPageBackgroundColor];
    
    //initialize groupMemberNames with the names ...
    groupMemberNames = [[NSMutableArray alloc] init];
    groupMemberPhoneNumbers = [[NSMutableArray alloc] init];
    for (Member *member in [selectedGroup members]){
        NSString *deepCopiedName = [[NSString alloc] initWithString:[member name]];
        [groupMemberNames addObject:deepCopiedName];
        
        NSString *deepCopiedNumber = [[NSString alloc] initWithString:[member phoneNumber]];
        [groupMemberPhoneNumbers addObject:deepCopiedNumber];
    }
    
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                              target:self action:@selector(addContact)];
    addButton.enabled = YES;
    self.navigationItem.rightBarButtonItem = addButton;
    
    NSArray* toolbarItems = [NSArray arrayWithObjects:
                             [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                           target:self
                                                                           action:@selector(cancelPressed)],
                             [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                           target:self
                                                                           action:@selector(donePressed)],
                             nil];
    self.toolbarItems = toolbarItems;
    self.navigationController.toolbarHidden = NO;
    
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [delegate willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}


- (void)addContact
{
    //init contacts object
    contacts = [[ABPeoplePickerNavigationController alloc] init];
    
    //set delegate
    [contacts setPeoplePickerDelegate:self];
    
    //display the contacts view controller
    [self presentViewController:contacts animated:YES completion:nil];
}

// Delegate method to make the Cancel button of the Address Book working.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [contacts dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    //get first and last name to compose new Member's name
    NSString *firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    NSString *fullName = @"";
    if (firstName != nil){
        fullName = [fullName stringByAppendingString:firstName];
    }
    if (lastName != nil){
        fullName = [fullName stringByAppendingString:@" "];
        fullName = [fullName stringByAppendingString:lastName];
    }

    NSArray *phones = (__bridge NSArray *)(ABMultiValueCopyArrayOfAllValues(ABRecordCopyValue(person, kABPersonPhoneProperty)));
    //NSString *mobile = nil;
    if ([phones count] > 0){
        [groupMemberNames insertObject:fullName atIndex:0];
        [groupMemberPhoneNumbers insertObject:[phones objectAtIndex:0] atIndex:0];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    //NSLog(@"count: %ld", CFArrayGetCount(phones));
    /*
    for(CFIndex i = 0; i < CFArrayGetCount(phones); i++) {
        NSString *mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phones, i);
        if([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel])
        {
            mobile = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
        }
    }
     */
 
    /*
    BOOL foundMobilePhone = NO;
    ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
    for(CFIndex i=0; i<ABMultiValueGetCount(phones); i++){
        NSString *mobileLabel = (__bridge NSString *)(ABMultiValueCopyLabelAtIndex(phones, i));
        if ([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel]){
            NSString *mobile = (__bridge NSString *)(ABMultiValueCopyValueAtIndex(phones, i));
            [groupMemberPhoneNumbers insertObject:mobile atIndex:0];
            foundMobilePhone = YES;
        }
    }
    
    if (foundMobilePhone){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } 
     */
     else {
        //TODO tell the user the contact couldn't be added because it has no mobile number.
        //... UIalertView?
    }
    
    [contacts dismissViewControllerAnimated:YES completion:nil];
    
    return NO;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    return NO;
}


- (void)donePressed
{
    //save changes !!
    //=> could be smart not to record a member in the DB each time it's created but, but just fill in an Array of Members and at the press on Done button, save everythong on the DB. Indeed, if you enter members, save them when you create them and then press cancel button you have to delete the new members in the DB !!!
    
    //Is anyone listening
    if ([delegate respondsToSelector:@selector(membersEdited:phones:)]) {
        //call the delegate method!
        [delegate membersEdited:groupMemberNames phones:groupMemberPhoneNumbers];
    }
}


- (void)cancelPressed
{
    //Is anyone listening
    if ([delegate respondsToSelector:@selector(canceledEditing)]) {
        //call the delegate method!
        [delegate canceledEditing];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (Group *)selectedGroup
{
    return selectedGroup;
}

- (void)setSelectedGroup:(Group *)group
{
    selectedGroup = group;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [groupMemberNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Configure the cell.
    [[cell textLabel] setText:[groupMemberNames objectAtIndex:indexPath.row]];
    [[cell detailTextLabel] setText:[groupMemberPhoneNumbers objectAtIndex:indexPath.row]];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Delete the member object at the given index path.
        [groupMemberNames removeObjectAtIndex:indexPath.row];
        [groupMemberPhoneNumbers removeObjectAtIndex:indexPath.row];
        
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  50.0;
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
