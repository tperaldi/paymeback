//
//  Member.h
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Group;

@interface Member : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phoneNumber;
@property (nonatomic, retain) Group *group;
@property (nonatomic, retain) NSSet *paidExpenses;
@property (nonatomic, retain) NSSet *participatedTo;
@end

@interface Member (CoreDataGeneratedAccessors)

- (void)addPaidExpensesObject:(NSManagedObject *)value;
- (void)removePaidExpensesObject:(NSManagedObject *)value;
- (void)addPaidExpenses:(NSSet *)values;
- (void)removePaidExpenses:(NSSet *)values;

- (void)addParticipatedToObject:(NSManagedObject *)value;
- (void)removeParticipatedToObject:(NSManagedObject *)value;
- (void)addParticipatedTo:(NSSet *)values;
- (void)removeParticipatedTo:(NSSet *)values;

@end
