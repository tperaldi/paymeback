//
//  Expense.m
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import "Expense.h"
#import "Group.h"
#import "Member.h"


@implementation Expense

@dynamic amount;
@dynamic date;
@dynamic name;
@dynamic group;
@dynamic participants;
@dynamic payer;

@end
