//
//  GroupListController.h
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"
#import "GroupDetailViewController.h"

@interface GroupListController : UITableViewController<UIAlertViewDelegate>
{
    Store *sharedStore;
    UIBarButtonItem *addButton;
    GroupDetailViewController *groupDetailViewController;
}

@property (nonatomic, retain) UIBarButtonItem *addButton;
- (void)addGroup;

@end
