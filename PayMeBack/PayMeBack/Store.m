//
//  Store.m
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import "Store.h"

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

@implementation Store

- (id)init
{
    self = [super init];
    if (!self)
        return self;
    
    //initialize store
    model = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    //create persistent store coordinator
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    NSURL *storeURL = [NSURL fileURLWithPath:self.archivePath];
    
    NSError *error = nil;
    if (![psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]){
        [NSException raise:@"Could not load stored data" format:@"Reason: %@", error.localizedDescription];
    }

    //set up the context
    context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = psc;
    context.undoManager = nil;
    
    //actually load data items from the disk.
    [self loadAllGroups];
    
    return self;
}

+ (Store *)sharedStore
{
    static Store *sharedStore = nil;
    if (!sharedStore)
        sharedStore = [[super allocWithZone:nil] init];
    
    return sharedStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedStore];
}

- (NSString *)archivePath
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = documentDirectories[0];
    return [documentDirectory stringByAppendingPathComponent:@"paymeback.data"]; //gives us the full path to wherever macOs decided to put our data !
}

- (void)loadAllGroups
{
    //check if the data have actually been loaded
    if (!allGroups) {
        //fetch the data
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Group"];
        
        //fetch also the subEntities... (YES might be the default value anyway)
        [request setIncludesSubentities:YES];
        
        //define a descriptor
        NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        
        //tell the request to use the descriptor we defined
        request.sortDescriptors = @[sd];
        
        //load data from the db
        NSError *error = nil;
        NSArray *result = [context executeFetchRequest:request error:&error];
        if (!result) {
            [NSException raise:@"Fetch failed" format:@"Reason: %@", error.localizedDescription];
        }
        
        //shallow copy (copying references => same objects!!)
        allGroups = [result mutableCopy];
    }
}

- (NSArray *)allGroups
{
    return allGroups;
}


- (BOOL)groupNameAlreadyExists:(NSString *)name
{
    for (Group *group in allGroups) {
        if ([[group name] isEqualToString:name]) {
            return YES;
        }
    }
    return NO;
}


- (Group *)insertGroupWithName:(NSString *)name
{
    
    if ([allTrim( name ) length] == 0) {
        return nil;
    }
    Group *newGroup = [NSEntityDescription insertNewObjectForEntityForName:@"Group" inManagedObjectContext:context];
    [newGroup setName:name];
    
    //[allGroups addObject:newGroup];
    //do we want to insert the new group at the beginning of the array? (probably best for display management)
    [allGroups insertObject:newGroup atIndex:0];
    
    return newGroup;
}

- (Member *)insertMemberWithName:(NSString *)name phone:(NSString *)phoneNumber inGroup:(Group *)group
{
    if ([allTrim( name ) length] == 0) {
        return nil;
    }
    Member *newMember = [NSEntityDescription insertNewObjectForEntityForName:@"Member" inManagedObjectContext:context];
    [newMember setName:name];
    [newMember setPhoneNumber:phoneNumber];
    [newMember setGroup:group];
    
    return newMember;
}

- (Expense *)insertExpenseWithName:(NSString *)name amount:(NSNumber *)amount date:(NSDate *)date payer:(Member *)payer participants:(NSArray *)participants group:(Group *)group
{
    Expense *newExpense = [NSEntityDescription insertNewObjectForEntityForName:@"Expense" inManagedObjectContext:context];
    [newExpense setName:name];
    [newExpense setAmount:amount];
    [newExpense setDate:date];
    [newExpense setGroup:group];
    [newExpense setPayer:payer];
    NSSet *participantsSet = [NSSet setWithArray:participants];
    [newExpense setParticipants:participantsSet];
    
    return newExpense;
}

//checks if member already exists in this group before inserting new member in DB.
- (Member *)addMember:(NSString *)name phone:(NSString *)phoneNumber toGroup:(Group *)group
{
    if ([self group:group hasMemberCalled:name] != nil){
        //the group in which we want to insert the new member already has a member called "name".
        return nil;
    }
    
    //no matching member's name was found in group: insert a new Member in DB.
    return [self insertMemberWithName:name phone:phoneNumber inGroup:group];
}

//returns a pointer to the matching member with name in group's members if it exists.
- (Member *)group:(Group *)group hasMemberCalled:(NSString *)name
{
    for (Member *member in [group members]){
        if ([[member name] isEqualToString:name]){
            return member;
        }
    }
    return nil;
}




- (void)deleteGroup:(Group *)group
{
    for (Member *member in [group members]){
        [self deleteMember:member fromGroup:group];
    }
    [context deleteObject:group];
    [allGroups removeObject:group]; //is it really necessary as allGroups is a shallow copy?? I don't think so!
}

//deletes a member FROM A GROUP IF it has paid no expenses OR if participated to no expenses in designated group
- (BOOL)deleteMember:(Member *)member fromGroup:(Group *)group
{
    if ([self member:member canBeDeletedFromGroup:group]){
        //ok, delete member (it will be effective when we save changes)
        [context deleteObject:member];
        return YES;
    }
    return NO;
}

- (BOOL)member:(Member *)member canBeDeletedFromGroup:(Group *)group
{
    if (([[member paidExpenses] count] == 0) && ([[member participatedTo] count] == 0)){
        return YES;
    }
    return NO;
}

//compares updatedMembersNames with [group members]names so [group members]names matches updatedMembersNames
- (void)updateMembersInGroup:(Group *)group withMembersNameList:(NSArray *)updatedMembersNames membersPhoneNumbers:(NSArray *)updatedPhoneNumbers
{
    //if a name is in [group member] and not in updatedMembersNames, delete this member from group
    for (Member *member in [group members]){
        if (! [self membersNamesArray:updatedMembersNames containsName:[member name]]) {
            [self deleteMember:member fromGroup:group];
        }
    }
    
    //if a member's name appears in updatedMembersNames but not in [group members], add this member in group
    for (NSString *updatedMemberName in updatedMembersNames){
        if (! [self membersSet:[group members] containsName:updatedMemberName]){
            [self insertMemberWithName:updatedMemberName phone:[updatedPhoneNumbers objectAtIndex:[updatedMembersNames indexOfObject:updatedMemberName]] inGroup:group];
        }
    }
}


- (BOOL)membersNamesArray:(NSArray *)membersNamesArray containsName:(NSString *)name
{
    for (NSString *setName in membersNamesArray){
        if ([setName isEqualToString:name]){
            return YES;
        }
    }
    return NO;
}

- (BOOL)membersSet:(NSSet *)membersSet containsName:(NSString *)name
{
    for (Member *member in membersSet){
        if ([[member name] isEqualToString:name]){
            return YES;
        }
    }
    return NO;
}



- (BOOL)saveChanges
{
    NSError *error = nil;
    BOOL success = [context save:&error];
    if (!success && [context hasChanges]) {
        // TODO: Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Error saving: %@", error.localizedDescription);
        abort();
    }
    return success;
}

@end
