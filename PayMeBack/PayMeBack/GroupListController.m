//
//  GroupListController.m
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import "GroupListController.h"
#import "GroupDetailViewController.h"

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

@interface GroupListController ()

@end


@implementation GroupListController


@dynamic addButton;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        groupDetailViewController = [[GroupDetailViewController alloc] initWithNibName:@"GroupDetailView" bundle:nil];
    }
    
    sharedStore =[Store sharedStore];
    
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Set the title.
    self.title = @"My Groups";
    
    //set background color
    self.tableView.backgroundColor = [UIColor underPageBackgroundColor];

    
    // Set up the buttons.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                              target:self action:@selector(addGroup)];
    addButton.enabled = YES;
    self.navigationItem.rightBarButtonItem = addButton;
}



- (void)addGroup
{    
    UIAlertView *groupInputAlert = [[UIAlertView alloc] initWithTitle:@"Add a Group" message:@"Type the name of the new group" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"save", nil];
    groupInputAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [groupInputAlert show];
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"%@", [alertView textFieldAtIndex:0].text);
    
    NSString *name = [alertView textFieldAtIndex:0].text;
    
    if (buttonIndex == 1 && [allTrim( name ) length] > 0) {
        //create a new instance of group
        if (![sharedStore groupNameAlreadyExists:name]){
            [sharedStore insertGroupWithName:name];
            
            //save changes
            [sharedStore saveChanges];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        } else {
            //TODO tell the user he can't add a group with already existing name (alert?) IF buttonIndex is not 0 (cancel button)
            //...
        }
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[sharedStore allGroups] count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    Group *group = (Group *)[[sharedStore allGroups] objectAtIndex:indexPath.row];

    [[cell textLabel] setText:[group name]];
    //UIFont *nameFont = [UIFont fontWithName:@"AmericanTypewriter" size:17.0];
    //[[cell textLabel] setFont:nameFont];
    //NSString *detail = [NSString stringWithFormat:@"%i", [[group members] count]];
    //detail = [detail stringByAppendingString:@" buddies to spam"];
    //[[cell detailTextLabel] setText:detail];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}



- (void)viewDidUnload {
    sharedStore = nil;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/




// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Delete the group object at the given index path.
        Group *groupToDelete = [[sharedStore allGroups] objectAtIndex:indexPath.row];
        [sharedStore deleteGroup:groupToDelete];
        //save changes
        [sharedStore saveChanges];
        
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}



/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/



/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[tableView deselectRowAtIndexPath:indexPath animated:NO]; //TODO or NOT TODO ?
    
    // Navigation logic may go here. Create and push another view controller.
    //GroupDetailViewController *groupDetailViewController = [[GroupDetailViewController alloc] initWithNibName:@"GroupDetailView" bundle:nil];

    [groupDetailViewController setSelectedGroup:[[sharedStore allGroups] objectAtIndex:indexPath.row]];
    
    
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:groupDetailViewController animated:YES];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [groupDetailViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

@end
