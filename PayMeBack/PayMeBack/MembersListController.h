//
//  MembersListController.h
//  PayMeBack
//
//  Created by tatiana peraldi on 21/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "Group.h"
#import "Store.h"
#import "Member.h"

@protocol MembersListDelegate <NSObject>
- (void)membersEdited:(NSArray *)updatedMembersNames phones:(NSArray *)updatedPhoneNumbers;
- (void)canceledEditing;
@end

@interface MembersListController : UITableViewController<ABPeoplePickerNavigationControllerDelegate>
{
    Group *selectedGroup;
    Store *sharedStore;
    NSMutableArray *groupMemberNames;
    NSMutableArray *groupMemberPhoneNumbers;

    UIBarButtonItem *addButton;
    //id<MembersListDelegate> delegate;
}

@property(nonatomic,assign)id delegate;
@property (nonatomic, retain) UIBarButtonItem *addButton;
@property (nonatomic, retain) ABPeoplePickerNavigationController *contacts;

- (Group *)selectedGroup;
- (void)setSelectedGroup:(Group *)group;
- (void)addContact;

- (void)donePressed;
- (void)cancelPressed;



@end

