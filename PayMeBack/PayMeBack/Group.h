//
//  Group.h
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Member;

@interface Group : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *expenses;
@property (nonatomic, retain) NSSet *members;
@end

@interface Group (CoreDataGeneratedAccessors)

- (void)addExpensesObject:(NSManagedObject *)value;
- (void)removeExpensesObject:(NSManagedObject *)value;
- (void)addExpenses:(NSSet *)values;
- (void)removeExpenses:(NSSet *)values;

- (void)addMembersObject:(Member *)value;
- (void)removeMembersObject:(Member *)value;
- (void)addMembers:(NSSet *)values;
- (void)removeMembers:(NSSet *)values;

@end
