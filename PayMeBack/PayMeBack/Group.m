//
//  Group.m
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import "Group.h"
#import "Member.h"


@implementation Group

@dynamic name;
@dynamic expenses;
@dynamic members;

@end
