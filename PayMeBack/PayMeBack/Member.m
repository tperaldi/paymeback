//
//  Member.m
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import "Member.h"
#import "Group.h"

@implementation Member

@dynamic name;
@dynamic phoneNumber;
@dynamic group;
@dynamic paidExpenses;
@dynamic participatedTo;

@end
