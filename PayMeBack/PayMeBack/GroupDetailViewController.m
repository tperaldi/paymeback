//
//  GroupDetailViewController.m
//  PayMeBack
//
//  Created by tatiana peraldi on 21/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import "GroupDetailViewController.h"

@interface GroupDetailViewController ()

@end

@implementation GroupDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        sharedStore =[Store sharedStore];
        
        //initialize spamTextChoices with the different spam messages.
        spamTextChoices = [[NSArray alloc] initWithObjects:
                           @"In the course of a lifetime the average person will grow 2 meters of nose hair.",
                           @"The average adult male shaves off 450grams of beard per year.",
                           @"Men can read smaller print than women; women can hear better.",
                           @"In Thailand, it is illegal to leave your house if you are not wearing underwear.",
                           @"An estimated one in five Americans – some 38 million – don’t like sex.",
                           @"More Monopoly money is printed in a year, than real money printed throughout the world!",
                           @"It takes 3,000 cows to supply the NFL with enough leather for a year’s supply of footballs.",
                           @"The first known contraceptive was crocodile dung used by ancient Egyptians in 2000 BC.",
                           @"The distance between an alligator’s eyes, in inches, is directly proportional to the length of the alligator in feet.",
                           @"When Joseph Gayetty invented toilet paper in 1857, he had his name printed on each sheet.",
                           @"Desperate to increase veggie intake among children in Britain, researchers have developed chocolate-flavored carrots, pizza-flavored corn and baked-bean-flavored peas.",
                           @"The cigarette lighter was invented before the match.",
                           @"In the 1800′s anyone in England who unsucessfully attempted suicide faced the death penalty.",
                           @"The house where Jefferson wrote the Declaration of Independence was replaced with a hamburger stand.",
                           @"The white powder on chewing gum is actually sweetened marble dust.",
                           @"During your lifetime, you’ll eat about 60,000 pounds of food, that’s the weight of about 6 elephants.",
                           @"Pound for pound (kilo for kilo), hamburgers cost more than new cars.",
                           @"The longest words that can be typed using only the right hand in proper typing form are ”lollipop” and “monopoly”.",
                           @"Check this out, look at your keyboard, the only ten letter word that you can spell with the top row of letters is “typewriter”.",
                           @"Skepticisms is the longest word that alternates hands when typing.",
                           @"A shrimp’s heart is located in it’s head.",
                           @"People began surfing in Hawaii before Columbus sailed to America.",
                           @"The acid in a person’s stomach is corrosive enough to melt iron.",
                           @"Most cows give more milk when they listen to music.",
                           @"Your eyeballs are three and a half percent salt.",
                           nil];
        UIFont *spamFont = [UIFont fontWithName:@"AmericanTypewriter" size:17.0];
        [self.spamText setFont:spamFont];
        [self.spamText setTextAlignment:NSTextAlignmentCenter];
    }
    return self;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if ((orientation == UIInterfaceOrientationPortrait) ||
        (orientation == UIInterfaceOrientationMaskLandscape))
        return YES;
    
    return NO;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@_landscape", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //set the view's title to be the group name
    [self setTitle:[selectedGroup name]];
    
    [self becomeFirstResponder];
    
    //customize buttons
    [self.editMembersButton showsTouchWhenHighlighted];
    [self.sendSpamButton showsTouchWhenHighlighted];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake)
    {
        // User was shaking the device. Post a notification named "shake."
        [[NSNotificationCenter defaultCenter] postNotificationName:@"shake" object:self];
        uint32_t rnd = arc4random_uniform([spamTextChoices count]);
        NSString *randomSpam = [spamTextChoices objectAtIndex:rnd];
        [self.spamText setText:randomSpam];
        UIFont *spamFont = [UIFont fontWithName:@"AmericanTypewriter" size:17.0];
        [self.spamText setFont:spamFont];
        [self.spamText setTextAlignment:NSTextAlignmentCenter];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (Group *)selectedGroup
{
    return selectedGroup;
}

- (void)setSelectedGroup:(Group *)group
{
    selectedGroup = group;
}

- (IBAction)editMembers:(id)sender {
    MembersListController *membersListController = [[MembersListController alloc] initWithStyle:UITableViewStylePlain];

    membersListController.delegate = self;
    [membersListController setSelectedGroup:selectedGroup];
    
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:membersListController];
    
    [[self navigationController] presentViewController:navigationController animated:YES completion: nil];
}

- (void)membersEdited:(NSArray *)updatedMembersNames phones:(NSArray *)updatedPhoneNumbers;
{
    [sharedStore updateMembersInGroup:selectedGroup withMembersNameList:updatedMembersNames membersPhoneNumbers:updatedPhoneNumbers];
    [sharedStore saveChanges];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)canceledEditing
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)sendSpam:(id)sender {
    
    if ([[selectedGroup members] count] == 0){
        [self showMessageSendingStateAlertWithTitle:@"Almost there!" body:@"No contacts to spam: edit contacts first, then try again."];
        return;
    }
    
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
	
	if (messageClass != nil) {
		// Check whether the current device is configured for sending SMS messages
		if ([messageClass canSendText]) {
			[self displaySMSComposerSheet];
		}
		else {
			[self showMessageSendingStateAlertWithTitle:@"Too bad..." body:@"Device not configured to send SMS."];
		}
	}
	else {
        [self showMessageSendingStateAlertWithTitle:@"Too bad..." body:@"Device not configured to send SMS."];
	}
}

-(void)displaySMSComposerSheet
{
    //get an Array of phoneNumbers!
    groupMemberPhoneNumbers = [[NSMutableArray alloc] init];
    for (Member *member in [selectedGroup members]){
        NSString *deepCopiedNumber = [[NSString alloc] initWithString:[member phoneNumber]];
        [groupMemberPhoneNumbers addObject:deepCopiedNumber];
    }
    
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
	picker.messageComposeDelegate = self;
    
    [picker setRecipients:groupMemberPhoneNumbers];
    [picker setBody:[[self spamText] text]];
	
	[self presentViewController:picker animated:YES completion:nil];
}

// Dismisses the message composition interface when users tap Cancel or Send.
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {

	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MessageComposeResultCancelled:
            [self showMessageSendingStateAlertWithTitle:@"Oops..." body:@"SMS sending canceled."];
			break;
		case MessageComposeResultSent:
            [self showMessageSendingStateAlertWithTitle:@"Spammed!!" body:@"Your've just spammed your contacts. Well done!"];
			break;
		case MessageComposeResultFailed:
			[self showMessageSendingStateAlertWithTitle:@"Oops..." body:@"SMS sending failed."];
			break;
		default:
			[self showMessageSendingStateAlertWithTitle:@"Oops..." body:@"SMS not sent."];
			break;
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showMessageSendingStateAlertWithTitle:(NSString *)title body:(NSString *)body
{
    UIAlertView *sendingStateAlert = [[UIAlertView alloc] initWithTitle:title message:body delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
    sendingStateAlert.alertViewStyle = UIAlertViewStyleDefault;
    [sendingStateAlert show];
}

@end
