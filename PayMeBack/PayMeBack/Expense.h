//
//  Expense.h
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Group, Member;

@interface Expense : NSManagedObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Group *group;
@property (nonatomic, retain) NSSet *participants;
@property (nonatomic, retain) Member *payer;
@end

@interface Expense (CoreDataGeneratedAccessors)

- (void)addParticipantsObject:(Member *)value;
- (void)removeParticipantsObject:(Member *)value;
- (void)addParticipants:(NSSet *)values;
- (void)removeParticipants:(NSSet *)values;

@end
