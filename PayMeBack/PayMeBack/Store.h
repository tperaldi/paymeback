//
//  Store.h
//  PayMeBack
//
//  Created by tatiana peraldi on 20/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Group.h"
#import "Member.h"
#import "Expense.h"

@interface Store : NSObject
{
    NSMutableArray *allGroups;
    NSManagedObjectContext *context;
    NSManagedObjectModel *model;
}

//singleton
+ (Store *)sharedStore;
+ (id)allocWithZone:(NSZone *)zone;

- (NSString *)archivePath;
- (void)loadAllGroups;
- (NSArray *)allGroups;

- (BOOL)groupNameAlreadyExists:(NSString *)name;

- (Group *)insertGroupWithName:(NSString *)name;
- (Member *)insertMemberWithName:(NSString *)name phone:(NSString *)phoneNumber inGroup:(Group *)group;
- (Expense *)insertExpenseWithName:(NSString *)name amount:(NSNumber *)amount date:(NSDate *)date payer:(Member *)payer participants:(NSArray *)participants group:(Group *)group;

- (Member *)addMember:(NSString *)name phone:(NSString *)phoneNumber toGroup:(Group *)group;
- (Member *)group:(Group *)group hasMemberCalled:(NSString *)name;

- (void)deleteGroup:(Group *)group;
- (BOOL)deleteMember:(Member *)member fromGroup:(Group *)group;
- (BOOL)member:(Member *)member canBeDeletedFromGroup:(Group *)group;

- (void)updateMembersInGroup:(Group *)group withMembersNameList:(NSArray *)updatedMembersNames membersPhoneNumbers:(NSArray *)updatedPhoneNumbers;
- (BOOL)membersNamesArray:(NSArray *)membersNamesArray containsName:(NSString *)name;
- (BOOL)membersSet:(NSSet *)membersSet containsName:(NSString *)name;

- (BOOL)saveChanges;

@end
