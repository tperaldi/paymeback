//
//  GroupDetailViewController.h
//  PayMeBack
//
//  Created by tatiana peraldi on 21/03/13.
//  Copyright (c) 2013 tatiana peraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Group.h"
#import "MembersListController.h"

@interface GroupDetailViewController : UIViewController<MembersListDelegate, MFMessageComposeViewControllerDelegate, UIAlertViewDelegate>
{
    Group *selectedGroup;
    Store *sharedStore;
    NSMutableArray *groupMemberPhoneNumbers;
    NSArray *spamTextChoices;
}
@property (weak, nonatomic) IBOutlet UIButton *editMembersButton;
@property (weak, nonatomic) IBOutlet UIButton *sendSpamButton;
@property (weak, nonatomic) IBOutlet UILabel *spamText;

- (Group *)selectedGroup;
- (void)setSelectedGroup:(Group *)group;
- (IBAction)editMembers:(id)sender;
- (IBAction)sendSpam:(id)sender;
- (void)displaySMSComposerSheet;
- (void)showMessageSendingStateAlertWithTitle:(NSString *)title body:(NSString *)body;

@end
